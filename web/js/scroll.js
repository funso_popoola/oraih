/**
 * Created by root on 5/19/15.
 */

$(window).scroll(function() {
    if ($(document).scrollTop() < 92) {
        $('#nav').removeClass('shrinked top-hidden transition-navbar').addClass('transition-navbar-top');
    } else {
        $('#nav').removeClass('transition-navbar-top').addClass('shrinked top-hidden transition-navbar navbar-transitioned');
    }
});

