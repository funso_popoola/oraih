<?php

use yii\db\Schema;
use yii\db\Migration;

class m150619_230801_remove_foodstuff_img_from_foodstuff_market extends Migration
{
    public function up()
    {
        $this->dropColumn(
            'foodstuff_markets',
            'foodstuff_market_image_url'
        );

        $this->addColumn(
            'foodstuffs',
            'foodstuff_image_url',
            'text not null'
        );
    }

    public function down()
    {
        echo "m150619_230801_remove_foodstuff_img_from_foodstuff_market cannot be reverted.\n";

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
