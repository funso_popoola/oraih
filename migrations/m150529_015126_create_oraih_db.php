<?php

use yii\db\Schema;
use yii\db\Migration;

class m150529_015126_create_oraih_db extends Migration
{
    public function up()
    {
        $sql = file_get_contents(__DIR__ . '/../data/schema.sql');
        $this->execute($sql);
    }

    public function down()
    {
        echo "m150529_015126_create_oraih_db cannot be reverted.\n";

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
