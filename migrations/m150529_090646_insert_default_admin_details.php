<?php

use yii\db\Schema;
use yii\db\Migration;

class m150529_090646_insert_default_admin_details extends Migration
{
    public function up()
    {
        $this->insert(
            'users',
            [
                'user_name' => 'admin',
                'user_email' => 'admin@oraih.com',
                'user_password' => Yii::$app->getSecurity()->generatePasswordHash('4dm1n'),
                'user_type_id' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'modified_at' => date('Y-m-d H:i:s'),
                'active_status' => 1,
            ]
        );
    }

    public function down()
    {
        $this->delete('users', ['user_name' => 'admin', 'user_email' => 'admin@oraih.com', 'user_type_id' => 1]);
//        echo "m150529_090646_insert_default_admin_details cannot be reverted.\n";

        return true;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
