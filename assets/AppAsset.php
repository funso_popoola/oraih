<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'z_theme/assets/css/theme.css',
        'z_theme/assets/css/carousel-animate.css',
        'z_theme/assets/css/magnific-popup.css',
        'z_theme/assets/css/owl.carousel.css',
        'z_theme/assets/css/owl.theme.css',
        'rrssb-master/css/rrssb.css',
        'http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,600,700,800,300',
        'http://netdna.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css',
    ];
    public $js = [
        'z_theme/assets/js/page/carousel-preload.js',
        'z_theme/assets/js/jquery.bxslider.js',
        'z_theme/assets/js/owl.carousel.js',
        'z_theme/assets/js/page/theme.js',
//        'z_theme/assets/js/page/page.shop-sidebar-and-item.js',
        'z_theme/js/bootstrap.min.js',
        'js/scroll.js',
        'rrssb-master/js/rrssb.min.js',
        'https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
