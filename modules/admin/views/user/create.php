<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 5/30/15
 * Time: 12:30 PM
 *
 * @var $userTypes mixed
 */
use yii\helpers\Url;
use yii\widgets\ActiveForm;

?>

<div class="col-md-9">
    <h1 class="title-v2"> Manage Sub-Admins </h1>

    <?= $this->render('form', ['model' => $model, 'userTypes' => $userTypes]) ?>
</div>

