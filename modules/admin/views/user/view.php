<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 5/30/15
 * Time: 10:54 AM
 *
 * @var $model \app\modules\admin\models\Users
 */
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

?>

<div class="col-md-9">
    <h1 class="title-v2"> Manage Sub-Admins </h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'user_name',
            'user_email',
            'userType.user_type_name',
            'active_status',
        ],
    ]) ?>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->user_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->user_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

</div>