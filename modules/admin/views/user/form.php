<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 5/30/15
 * Time: 11:13 AM
 *
 * @var $model \app\modules\admin\models\Users
 * @var $userTypes mixed
 */
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<?php $form = ActiveForm::begin() ?>

    <?= $form->field($model, 'user_name')->textInput() ?>
    <?= $form->field($model, 'user_email')->input('email') ?>
    <?php if ($model->scenario == 'create'): ?>
        <?= $form->field($model, 'user_password')->passwordInput() ?>
    <?php endif; ?>
    <?= $form->field($model, 'user_type_id')->dropDownList($userTypes)->label('User Type') ?>
    <?php if ($model->scenario == 'update'): ?>
        <?= $form->field($model, 'active_status')->dropDownList([1 => 'ACTIVE', 0 => 'BLOCKED'])->label('Status') ?>
    <?php endif; ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

<?php ActiveForm::end() ?>