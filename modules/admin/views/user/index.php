<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 5/19/15
 * Time: 9:03 PM
 *
 * @var $searchModel \app\modules\admin\models\UserSearch
 * @var $dataProvider \yii\data\ActiveDataProvider
 */
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

?>
<div class="col-md-9">
    <h1 class="title-v2"> Manage Sub-Admins </h1>

    <div>
        <?= Html::a('Create User', ['create'], ['class' => 'btn btn-success']) ?>
    </div>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'user_name',
            'user_email',
            'userType.user_type_name',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]);

    ?>
</div>