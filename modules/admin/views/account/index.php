<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 5/19/15
 * Time: 9:03 PM
 *
 * @var $this yii\web\View
 * @var $model \app\modules\admin\models\Users
 */
use yii\helpers\Url;

?>
<div class="col-md-9">
    <h1 class="title-v2"> Manage Account </h1>
    <?php if (Yii::$app->session->hasFlash('successful')): ?>
        <div class="alert alert-success">
            Feedback: <?= Yii::$app->session->getFlash('successful') ?>
        </div>
    <?php elseif (Yii::$app->session->hasFlash('error')): ?>
        <div class="alert alert-danger">
            Feedback: <?= Yii::$app->session->getFlash('error') ?>
        </div>
    <?php endif; ?>
    <?= $this->render('form', ['model' => $model]); ?>
</div>