<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 5/29/15
 * Time: 12:37 PM
 *
 * @var $model \app\modules\admin\models\Users
 */
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<div class="col-md-6">
    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'oldPassword')->passwordInput() ?>
        <?= $form->field($model, 'newPassword')->passwordInput() ?>
        <div class="form-group">
            <?= Html::submitButton('Change Password', ['class' => 'btn btn-success']) ?>
        </div>

    <?php ActiveForm::end(); ?>
</div>