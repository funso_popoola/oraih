<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 5/30/15
 * Time: 11:13 AM
 *
 * @var $model \app\models\Foodstuffs
 * @var $categories mixed
 */
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

?>

<?php $form = ActiveForm::begin([
    'options' => [
        'enctype' => 'multipart/form-data'
    ],
]) ?>

    <?= $form->field($model, 'foodstuff_name')->textInput() ?>
    <?= $form->field($model, 'foodstuff_desc')->textarea() ?>
    <?= $form->field($model, 'category_id')->dropDownList($categories)->label('Category') ?>

    <?php if (!$model->isNewRecord && $model->foodstuff_image_url != ""): ?>
        <?= Html::img(Yii::$app->urlManager->createAbsoluteUrl('uploads/' .$model->foodstuff_image_url), ['width' => '100px', 'height' => '100px']) ?>
    <?php endif; ?>
    <?= $form->field($model, 'file')->fileInput() ?>

    <?= $form->field($model, 'active_status')->dropDownList([1 => 'ACTIVE', 0 => 'BLOCKED'])->label('Status') ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

<?php ActiveForm::end() ?>