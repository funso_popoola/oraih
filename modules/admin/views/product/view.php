<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 5/30/15
 * Time: 10:54 AM
 *
 * @var $model \app\models\Foodstuffs
 */
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

?>

<div class="col-md-9">
    <h1 class="title-v2"> Manage Sub-Admins </h1>

    <div class="col-md-3 img-responsive">
        <?php $img_url = ($model->foodstuff_image_url == "") ? '/z_theme/assets/images/shop/500x500.gif' : ('uploads/' . $model->foodstuff_image_url); ?>
        <?= Html::img(Yii::$app->urlManager->createAbsoluteUrl($img_url), ['width' => '150px', 'height' => '150px']) ?>

    </div>
    <div class="col-md-9">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'foodstuff_name',
                'foodstuff_desc',
                'category.category_name',
                'active_status',
            ],
        ]) ?>
    </div>
    <div class="col-md-12">
        <p>
            <?= Html::a('Update', ['update', 'id' => $model->foodstuff_id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->foodstuff_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
        </p>
    </div>





</div>