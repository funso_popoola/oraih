<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 5/19/15
 * Time: 9:03 PM
 *
 * @var $dataProvider \yii\data\ActiveDataProvider
 * @var $searchModel \app\models\FoodstuffSearch
 */
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

?>

<div class="col-md-9">
    <h1 class="title-v2"> Foodstuffs </h1>

    <div>
        <?= Html::a('Create Foodstuff', ['create'], ['class' => 'btn btn-success']) ?>
    </div>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'foodstuff_name',
            'foodstuff_desc',
            'category.category_name',
            'active_status',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]);

    ?>
</div>