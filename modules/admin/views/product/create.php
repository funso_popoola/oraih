<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 5/30/15
 * Time: 12:30 PM
 * @var $model \app\models\Foodstuffs
 * @var $categories mixed
 */
use yii\helpers\Url;

?>

<div class="col-md-9">
    <h1 class="title-v2"> Foodstuffs </h1>


    <?= $this->render('form', ['model' => $model, 'categories' => $categories]) ?>
</div>

