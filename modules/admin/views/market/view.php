<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 5/30/15
 * Time: 10:54 AM
 *
 * @var $model \app\models\Markets
 * @var $dataProvider \yii\data\ActiveDataProvider
 * @var $searchModel \app\models\FoodstuffMarkets
 */
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

?>

<div class="col-md-9">
    <h1 class="title-v2"> Manage <?= $model->market_name ?> </h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'market_name',
            'market_address',
            'market_contacts',
            'active_status',
        ],
    ]) ?>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->market_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->market_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <div class="title-md text-center title-striped">
        Foodstuffs in <?= $model->market_name ?>
    </div>

    <div>
        <?= Html::a('Add Foodstuff', ['/admin/market/create-foodstuff', 'id' => $model->market_id], ['class' => 'btn btn-success']) ?>
    </div>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'foodstuff.foodstuff_name',
            'foodstuff_market_price',
            'foodstuffMarketPriceMeasure.measure_name',

            ['class' => 'yii\grid\ActionColumn',
            'buttons' => [
                'view' => function ($url, $model, $key) {
                    $options = array_merge([
                        'title' => Yii::t('yii', 'View'),
                        'aria-label' => Yii::t('yii', 'View'),
                        'data-pjax' => '0',
                    ]);
                    return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', Yii::$app->urlManager->createUrl(['/admin/market/view-foodstuff', 'id' => $model->foodstuff_market_id]), $options);
                },
                'update' => function ($url, $model, $key) {
                    $options = array_merge([
                        'title' => Yii::t('yii', 'Update'),
                        'aria-label' => Yii::t('yii', 'Update'),
                        'data-pjax' => '0',
                    ]);
                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', Yii::$app->urlManager->createUrl(['/admin/market/update-foodstuff', 'id' => $model->foodstuff_market_id]), $options);
                },
                'delete' => function ($url, $model, $key) {
                    $options = array_merge([
                        'title' => Yii::t('yii', 'Delete'),
                        'aria-label' => Yii::t('yii', 'Delete'),
                        'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                        'data-method' => 'post',
                        'data-pjax' => '0',
                    ]);
                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', Yii::$app->urlManager->createUrl(['/admin/market/delete-foodstuff', 'id' => $model->foodstuff_market_id]), $options);
                }
            ],

            ],
        ],
    ]);
    ?>


    <div>

    </div>

</div>