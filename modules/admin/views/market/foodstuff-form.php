<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 5/30/15
 * Time: 11:13 AM
 *
 * @var $model \app\models\FoodstuffMarkets
 * @var $foodstuffs mixed
 * @var $market_id integer
 * @var $measures mixed
 */
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

?>

<?php $form = ActiveForm::begin() ?>

    <?= $form->field($model, 'foodstuff_id')->dropDownList($foodstuffs) ?>
    <div class="hidden">
        <?= $form->field($model, 'market_id')->textInput(['value' => $market_id])->hiddenInput() ?>
    </div>
    <?= $form->field($model, 'foodstuff_market_price')->textInput() ?>
    <?= $form->field($model, 'foodstuff_market_price_measure_id')->dropDownList($measures) ?>
    <?= $form->field($model, 'active_status')->dropDownList([1 => 'ACTIVE', 0 => 'BLOCKED'])->label('Status') ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

<?php ActiveForm::end() ?>