<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 5/30/15
 * Time: 12:30 PM
 *
 */
use yii\helpers\Url;

?>

<div class="col-md-9">
    <h1 class="title-v2"> Create Market </h1>

    <?= $this->render('form', ['model' => $model]) ?>
</div>

