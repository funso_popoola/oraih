<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 5/30/15
 * Time: 12:30 PM
 *
 * @var $model app\models\FoodstuffMarkets
 * @var $foodstuffs mixed
 * @var $market app\models\Markets
 * @var $measures mixed
 *
 */
use yii\helpers\Url;

?>

<div class="col-md-9">
    <h1 class="title-v2"> Create Foodstuff in <?= $market->market_name ?> </h1>

    <?php if (Yii::$app->session->hasFlash('creationError')) : ?>
        <div class="alert alert-danger">
            Error: <?= Yii::$app->session->getFlash('creationError') ?>
        </div>
    <?php endif; ?>
    <?= $this->render('foodstuff-form', [
        'model' => $model,
        'market_id' => $market->market_id,
        'foodstuffs' => $foodstuffs,
        'measures' => $measures,
    ]) ?>
</div>

