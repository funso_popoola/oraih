<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 5/30/15
 * Time: 10:54 AM
 *
 * @var $model \app\models\FoodstuffMarkets
 */
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

?>

<div class="col-md-9">
    <h1 class="title-v2"> Markets </h1>

    <div class="col-md-3 img-responsive">
        <?php $img_url = ($model->foodstuff->foodstuff_image_url == "") ? '/z_theme/assets/images/shop/500x500.gif' : ('uploads/' . $model->foodstuff->foodstuff_image_url); ?>
        <?= Html::img(Url::to(Yii::$app->request->baseUrl . $img_url), ['width' => '150px', 'height' => '150px']) ?>

    </div>

    <div class="col-md-9">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'foodstuff.foodstuff_name',
                'foodstuff_market_price',
                'foodstuffMarketPriceMeasure.measure_name',
                'active_status',
            ],
        ]) ?>
    </div>

    <div class="col-md-12">
        <p>
            <?= Html::a('Update', ['update-foodstuff', 'id' => $model->foodstuff_market_id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete-foodstuff', 'id' => $model->foodstuff_market_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
        </p>

    </div>



    <div>

    </div>

</div>