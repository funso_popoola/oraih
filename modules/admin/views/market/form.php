<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 5/30/15
 * Time: 11:13 AM
 *
 * @var $model \app\modules\admin\models\Users
 * @var $userTypes mixed
 */
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<?php $form = ActiveForm::begin() ?>

    <?= $form->field($model, 'market_name')->textInput() ?>
    <?= $form->field($model, 'market_address')->textarea() ?>
    <?= $form->field($model, 'market_contacts')->textarea() ?>
    <?= $form->field($model, 'active_status')->dropDownList([1 => 'ACTIVE', 0 => 'BLOCKED'])->label('Status') ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

<?php ActiveForm::end() ?>