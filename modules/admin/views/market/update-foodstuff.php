<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 5/30/15
 * Time: 11:07 AM
 *
 * @var $model app\models\FoodstuffMarkets
 * @var $markets mixed
 * @var $foodstuffs mixed
 * @var $measures mixed
 */
use yii\helpers\Url;

?>

<div class="col-md-9">
    <h1 class="title-v2"> Update Foodstuff in <?= $model->market->market_name?> </h1>

    <?= $this->render('foodstuff-form', ['model' => $model, 'foodstuffs' => $foodstuffs, 'market_id' => $model->market_id, 'measures' => $measures]) ?>
</div>