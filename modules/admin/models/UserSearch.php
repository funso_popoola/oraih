<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 5/29/15
 * Time: 10:55 AM
 */

namespace app\modules\admin\models;


use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class UserSearch extends Users{
    public function attributes()
    {
        return array_merge(parent::attributes(), ['userType.user_type_name']);
    }


    public function rules()
    {
        return [
            [['user_name', 'user_email', 'userType.user_type_name'], 'safe'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Users::find()
            ->innerJoinWith('userType')
            ->where('user_id != ' . Yii::$app->user->getId())
            ->andWhere('users.user_type_id != 1');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 5
            ],
        ]);

        $this->load($params);
        if (!$this->validate()){
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'user_name', $this->user_name])
            ->andFilterWhere(['like', 'user_email', $this->user_email])
            ->andFilterWhere(['user_types.user_type_name' => $this->getAttribute('userType.user_type_name')]);
        return $dataProvider;
    }
}