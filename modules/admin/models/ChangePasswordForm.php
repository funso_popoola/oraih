<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 6/20/15
 * Time: 1:38 AM
 *
/* @var $identity \app\modules\admin\models\Users
 */


namespace app\modules\admin\models;


use Yii;
use yii\base\Model;

class ChangePasswordForm extends Model{

    public $oldPassword;
    public $newPassword;

    public function rules()
    {
        return [
            [['oldPassword', 'newPassword'], 'required'],
            ['oldPassword', 'validatePassword'],
        ];
    }

    public function validatePassword($attributes, $params)
    {
        $identity = Yii::$app->user->identity;
        if (!Yii::$app->getSecurity()->validatePassword($this->oldPassword, $identity->user_password)){
            $this->addError($attributes, 'The old password is incorrect!');
        }
    }

    public function changePassword()
    {
        return $this->validate();
    }


}