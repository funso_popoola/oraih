<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 5/27/15
 * Time: 9:06 AM
 */

namespace app\modules\admin\controllers;

use app\models\Categories;
use app\models\Foodstuffs;
use app\models\FoodstuffSearch;
use Yii;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class ProductController extends Controller{

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionIndex(){
        $this->ensureLogin();

        $searchModel = new FoodstuffSearch();
        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);
        return $this->render('index', ['searchModel' => $searchModel, 'dataProvider' => $dataProvider]);
    }

    public function actionView($id)
    {
        $this->ensureLogin();

        $model = Foodstuffs::findOne($id);
        return $this->render('view', ['model' => $model]);
    }

    public function actionCreate(){
        $this->ensureLogin();

        $model = new Foodstuffs();

        $categories = Categories::find()->all();
        $categoriesArr = [];
        foreach ($categories as $type){
            $categoriesArr[$type->category_id] = $type->category_name;
        }


        if ($model->load(Yii::$app->request->post())){

            if (Yii::$app->request->isPost){
                $model->uploadFile();
            }

            if ($model->save()) {
                $model->created_at = date('Y-m-d H:i:s');
                $model->modified_at = date('Y-m-d H:i:s');
                $model->update();
                return $this->redirect(['view', 'id' => $model->foodstuff_id]);
            }
        }
        return $this->render('create', [
            'model' => $model,
            'categories' => $categoriesArr
        ]);
    }

    public function actionUpdate($id){

        $this->ensureLogin();

        $model = $this->findModel($id);

        $categories = Categories::find()->all();
        $categoriesArr = [];
        foreach ($categories as $type){
            $categoriesArr[$type->category_id] = $type->category_name;
        }

        if ($model->load(Yii::$app->request->post())){
            if (Yii::$app->request->isPost){
                $model->uploadFile();
            }
            $model->modified_at = date('Y-m-d H:i:s');
            if ($model->update()) {
                return $this->redirect(['view', 'id' => $model->foodstuff_id]);
            }
        }
        return $this->render('update', [
            'model' => $model,
            'categories' => $categoriesArr
        ]);
    }

    public function actionDelete($id){
        $this->ensureLogin();

        $this->findModel($id)->delete();
        return $this->redirect(['/admin/product']);
    }

    /**
     * Finds the Users model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Foodstuffs the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Foodstuffs::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function ensureLogin()
    {
        if (Yii::$app->user->isGuest){
            return $this->redirect(['/site/login']);
        }
    }
}