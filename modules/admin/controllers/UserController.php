<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 5/27/15
 * Time: 9:15 AM
 */

namespace app\modules\admin\controllers;


use app\modules\admin\models\Users;
use app\modules\admin\models\UserSearch;
use app\modules\admin\models\UserTypes;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

class UserController extends Controller{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['get'],
                    'delete' => ['post']
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex(){
        $this->ensureLogin();
        if (Yii::$app->user->getIdentity()->user_type_id != 1){
            throw new ForbiddenHttpException('This is exclusively for the admin!');
        }
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', ['dataProvider' => $dataProvider, 'searchModel' => $searchModel]);
    }

    public function actionView($id)
    {
        $this->ensureLogin();
        if (Yii::$app->user->getIdentity()->user_type_id != 1){
            throw new ForbiddenHttpException('This is exclusively for the admin!');
        }
        $model = Users::findOne($id);
        return $this->render('view', ['model' => $model]);
    }

    public function actionCreate()
    {
        $this->ensureLogin();

        if (Yii::$app->user->getIdentity()->user_type_id != 1){
            throw new ForbiddenHttpException('This is exclusively for the admin!');
        }

        $model = new Users();
        $model->scenario = 'create';

        $userTypes = UserTypes::find()->all();
        $userTypesArr = [];
        foreach ($userTypes as $type){
            $userTypesArr[$type->user_type_id] = $type->user_type_name;
        }

        if ($model->load(Yii::$app->request->post())){
            if ($model->save()) {
                $model->created_at = date('Y-m-d H:i:s');
                $model->modified_at = date('Y-m-d H:i:s');
                $model->user_password = Yii::$app->getSecurity()->generatePasswordHash($model->user_password);
                $model->update();
                return $this->redirect(['view', 'id' => $model->user_id]);
            }
        }
        return $this->render('create', [
            'model' => $model,
            'userTypes' => $userTypesArr
        ]);
    }

    public function actionUpdate($id)
    {
        $this->ensureLogin();

        if (Yii::$app->user->getIdentity()->user_type_id != 1){
            throw new ForbiddenHttpException('This is exclusively for the admin!');
        }

        $model = $this->findModel($id);
        $model->scenario = 'update';
        $model->modified_at = date('Y-m-d H:i:s');

        $userTypes = UserTypes::find()->all();
        $userTypesArr = [];
        foreach ($userTypes as $type){
            $userTypesArr[$type->user_type_id] = $type->user_type_name;
        }

        if ($model->load(Yii::$app->request->post())){
            $model->modified_at = date('Y-m-d H:i:s');
            if ($model->update()) {
                return $this->redirect(['view', 'id' => $model->user_id]);
            }
        }
        return $this->render('update', [
            'model' => $model,
            'userTypes' => $userTypesArr
        ]);
    }

    public function actionDelete($id)
    {
        $this->ensureLogin();

        if (Yii::$app->user->getIdentity()->user_type_id != 1){
            throw new ForbiddenHttpException('This is exclusively for the admin!');
        }

        $this->findModel($id)->delete();
        return $this->redirect(['/admin/user']);
    }

    /**
     * Finds the Users model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Users the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Users::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function ensureLogin()
    {
        if (Yii::$app->user->isGuest){
            return $this->redirect(['/site/login']);
        }
    }
}