<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 5/27/15
 * Time: 10:14 AM
 */

namespace app\modules\admin\controllers;


use app\modules\admin\models\ChangePasswordForm;
use app\modules\admin\models\Users;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class AccountController extends Controller{

    public function actionIndex(){

        $this->ensureLogin();

        $model = $this->findModel(Yii::$app->user->getId());
        $model->scenario = 'profile';
        $formModel = new ChangePasswordForm();

        if ($formModel->load(Yii::$app->request->post()) && $formModel->changePassword()){
            $model->modified_at = date('Y-m-d H:i:s');
            $model->user_password = Yii::$app->getSecurity()->generatePasswordHash($formModel->newPassword);
            if ($model->update()) {
                Yii::$app->session->setFlash('successful', 'Credentials Updated!');
                return $this->refresh();
            }
            else{
                Yii::$app->session->setFlash('error', 'Something went wrong; please try again!');
                return $this->refresh();
            }
        }

        return $this->render('index', ['model' => $formModel]);
    }

//    public function actionUpdate(){
//        if (Yii::$app->user->isGuest){
//            return $this->redirect(['/admin/user/login']);
//        }
//
//        return $this->render('update');
//    }

    /**
     * Finds the Users model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Users the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Users::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function ensureLogin()
    {
        if (Yii::$app->user->isGuest){
            return $this->redirect(['/site/login']);
        }
    }
}