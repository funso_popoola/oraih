<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 5/27/15
 * Time: 9:03 AM
 */

namespace app\modules\admin\controllers;

use app\models\FoodstuffMarkets;
use app\models\FoodstuffMarketSearch;
use app\models\Foodstuffs;
use app\models\Markets;
use app\models\MarketSearch;
use app\models\Measures;
use Yii;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class MarketController extends Controller{

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionIndex(){
        $this->ensureLogin();

        $searchModel = new MarketSearch();
        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);

        return $this->render('index', ['dataProvider' => $dataProvider, 'searchModel' => $searchModel]);
    }

    public function actionView($id){
        $this->ensureLogin();

        $model = Markets::findOne($id);

        $searchModel = new FoodstuffMarketSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $id);

        return $this->render('view', ['model' => $model, 'searchModel' => $searchModel, 'dataProvider' => $dataProvider]);
    }

    public function actionViewFoodstuff($id){
        $this->ensureLogin();

        $model = $this->findFoodstuffModel($id);
        return $this->render('view-foodstuff', ['model' => $model]);
    }

    public function actionCreate(){
        $this->ensureLogin();

        $model = new Markets();

        if ($model->load(Yii::$app->request->post())){
            if ($model->save()) {
                $model->created_at = date('Y-m-d H:i:s');
                $model->modified_at = date('Y-m-d H:i:s');
                $model->update();
                return $this->redirect(['view', 'id' => $model->market_id]);
            }
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionCreateFoodstuff($id){
        $this->ensureLogin();

        $model = new FoodstuffMarkets();

        if ($model->load(Yii::$app->request->post())){

            if ($model->isDuplicate()){
                Yii::$app->session->setFlash('creationError', 'This foodstuff already exists in this market!');
                $this->refresh();
            }

            if ($model->save()) {
                $model->created_at = date('Y-m-d H:i:s');
                $model->modified_at = date('Y-m-d H:i:s');
                $model->update();
                return $this->redirect(['view-foodstuff', 'id' => $model->foodstuff_market_id]);
            }
        }
        return $this->render('create-foodstuff', [
            'model' => $model,
            'foodstuffs' => $this->getFoodstuffs(),
            'market' => Markets::findOne($id),
            'measures' => $this->getMeasures(),
        ]);
    }

    public function actionUpdate($id){
        $this->ensureLogin();

        $model = $this->findModel($id);
        $model->modified_at = date('Y-m-d H:i:s');

        if ($model->load(Yii::$app->request->post())){
            $model->modified_at = date('Y-m-d H:i:s');
            if ($model->update()) {
                return $this->redirect(['view', 'id' => $model->market_id]);
            }
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionUpdateFoodstuff($id){
        $this->ensureLogin();

        $model = $this->findFoodstuffModel($id);
        $model->modified_at = date('Y-m-d H:i:s');

        if ($model->load(Yii::$app->request->post())){

            $model->modified_at = date('Y-m-d H:i:s');
            if ($model->update()) {
                return $this->redirect(['view-foodstuff', 'id' => $model->foodstuff_market_id]);
            }
        }
        return $this->render('update-foodstuff', [
            'model' => $model,
            'foodstuffs' => $this->getFoodstuffs(),
            'measures' => $this->getMeasures(),
        ]);
    }

    public function actionDelete($id){
        $this->ensureLogin();

        $this->findModel($id)->delete();
        return $this->redirect(['/admin/market']);
    }

    public function actionDeleteFoodstuff($id){
        $this->ensureLogin();

        $this->findFoodstuffModel($id)->delete();
        return $this->redirect(['/admin/foodstuff-market']);
    }

    /**
     * Finds the Users model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Markets the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Markets::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Finds the Users model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return FoodstuffMarkets the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findFoodstuffModel($id)
    {
        if (($model = FoodstuffMarkets::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function ensureLogin()
    {
        if (Yii::$app->user->isGuest){
            return $this->redirect(['/site/login']);
        }
    }

    public function getFoodstuffs()
    {
        $foodstuffModel = Foodstuffs::find()->all();
        $foodstuff = [];

        foreach($foodstuffModel as $food){
            $foodstuff[$food['foodstuff_id']] = $food['foodstuff_name'];
        }

        return $foodstuff;
    }

    public function getMarkets()
    {
        $marketModel = Markets::find()->all();
        $markets = [];

        foreach($marketModel as $mart){
            $markets[$mart['market_id']] = $mart['market_name'];
        }

        return $markets;
    }

    public function getMeasures()
    {
        $measuresModel = Measures::find()->all();
        $measures = [];

        foreach($measuresModel as $measure){
            $measures[$measure['measure_id']] = $measure['measure_name'];
        }

        return $measures;
    }

}