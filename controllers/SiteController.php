<?php

namespace app\controllers;

use app\models\FoodstuffMarkets;
use Yii;
use yii\data\Pagination;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\modules\admin\models\Users;
use app\models\ContactForm;

class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
//            'verbs' => [
//                'class' => VerbFilter::className(),
//                'actions' => [
//                    'logout' => ['post'],
//                ],
//            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->redirect(['/admin/market']);
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect(['/admin/market']);
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->redirect(['/site/index']);
    }


    public function actionIndex()
    {

        $totalFoodstuffCount = FoodstuffMarkets::find()->count();

        $max = $totalFoodstuffCount;
        if ($totalFoodstuffCount > 24){
            $max = 24;
        }
        $pagination = new Pagination(
            [
                'defaultPageSize' => 12,
                'totalCount' => $max
            ]
        );


        $foodstuffs = (new Query())
            ->select(['foodstuff_markets.*', 'foodstuffs.*', 'categories.*', 'measures.*'])
            ->from(['foodstuff_markets', 'foodstuffs', 'categories', 'measures'])
            ->where('foodstuff_markets.foodstuff_id = foodstuffs.foodstuff_id')
            ->andWhere('foodstuffs.category_id=categories.category_id')
            ->andWhere('foodstuff_markets.foodstuff_market_price_measure_id=measures.measure_id')
            ->orderBy(['foodstuff_markets.modified_at' => SORT_DESC])
            ->groupBy('foodstuff_market_id')
            ->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();

        $cheapestFoodstuffs = FoodstuffMarkets::getCheapestFoodstuffs(5);

        return $this->render('index', ['cheapestFoodstuffs' => $cheapestFoodstuffs, 'model'=>$foodstuffs, 'pagination' => $pagination]);
    }

//    public function actionContact()
//    {
//        $model = new ContactForm();
//        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
//            Yii::$app->session->setFlash('contactFormSubmitted');
//
//            return $this->refresh();
//        } else {
//            return $this->render('contact', [
//                'model' => $model,
//            ]);
//        }
//    }

    public function actionSearch($key = null)
    {
        $model = FoodstuffMarkets::searchFoodstuffs($key);

        return $this->render('search', ['model' => $model]);
    }

//    public function actionAbout()
//    {
//        return $this->render('about');
//    }
}
