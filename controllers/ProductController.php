<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 5/19/15
 * Time: 8:30 PM
 */

namespace app\controllers;


use app\models\Categories;
use app\models\FoodstuffMarkets;
use app\models\Foodstuffs;
use yii\data\Pagination;
use yii\db\Query;
use yii\web\Controller;

class ProductController extends Controller{

    public function actionIndex(){
        $categories = (new Categories())->find()->all();
        return $this->render('index', ['categories' => $categories]);
    }

    public function actionCategory($id){
        $categories = (new Categories())->find()->all();
        $selectedCategory = (new Categories())->findOne($id);
        return $this->render('index', ['categories' => $categories,'selectedCategory' => $selectedCategory]);
    }

    public function actionPrices($cat_id, $prod_id){
        $query = FoodstuffMarkets::getFoodstuffPrices(intval($prod_id));

        $others = (new Foodstuffs())->find()->where(['category_id' => $cat_id])->all();

        return $this->render('prices', ['model' => $query, 'others' => $others]);
    }

    /**
     * @param $food_market_id
     * @return string
     */
    public function actionItem($food_market_id){

        $foodstuffMarket = (new FoodstuffMarkets())->findOne($food_market_id);

        $foodstuff = $foodstuffMarket->getFoodstuff()->limit(1)->one();
        $selectedMarket = $foodstuffMarket->getMarket()->limit(1)->one();
        $measure = $foodstuffMarket->getFoodstuffMarketPriceMeasure()->limit(1)->one();

        $markets = FoodstuffMarkets::getSpecificSuppliers($foodstuff->foodstuff_id);

        $table_data = FoodstuffMarkets::getFoodstuffPriceCompare($foodstuff->foodstuff_id);


        return $this->render('item', ['markets' => $markets, 'tableData' => $table_data, 'selectedMarket' => $selectedMarket, 'foodstuff' => $foodstuff, 'foodstuffMarket' => $foodstuffMarket, 'measure' => $measure]);
    }
}