<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 5/20/15
 * Time: 10:33 PM
 */

namespace app\controllers;


use app\models\Categories;
use app\models\Markets;
use yii\db\Query;
use yii\web\Controller;

class MarketController extends Controller{

    public function actionIndex($id = null, $cat_id = null){

        if ($id != null){
            $firstMarketId = $id;
        }
        else{
            $markets = Markets::find()->limit(1)->one();
            $firstMarketId = $markets->market_id;
        }

        $market = (new Markets())->findOne($firstMarketId);

        $categories = Markets::getValidCategories($firstMarketId);

        if ($cat_id != null){
            $categoryId = $cat_id;
        }
        else{
            $categoryId = $categories[0]["category_id"];
        }

        $selectedCategory = (new Categories())->findOne($categoryId);

        $model = Markets::getMarketByCategoryId($firstMarketId, $categoryId);

        $allMarkets = Markets::find()->all();

        return $this->render('index', ['categories' => $categories, 'model' => $model, 'selectedCategory' => $selectedCategory, 'market' => $market, 'allMarkets' => $allMarkets]);
    }
}