<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 5/20/15
 * Time: 10:35 PM
 *
 * @var $model mixed
 * @var $categories mixed
 * @var $selectedCategory \app\models\Categories
 * @var $market \app\models\Markets
 * @var $allMarkets \app\models\Markets []
 */
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Oraih';
$this->params['breadcrumbs'][] = $this->title;
$row_begin = '<div class="row mt-20">';
$row_end = '</div>';
$size = count($model);
?>

<div class="container mt">
      <div class="row">
        <!-- SIDEBAR -->
        <!--===============================================================-->
        <div class="col-md-3">
          <div class="row-heading">
            <div class="col-sm-12">
              <h3 class="title-sm hr-left text-uppercase">Foodstuff Categories</h3>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default">

                    <div class="list-group">

                        <?php foreach($categories as $category): ?>
                            <a href="<?= Url::to(['market/index', 'id' => $market->market_id, 'cat_id' => $category["category_id"]])?>" class="<?= 'list-group-item' . (($category["category_id"] == $selectedCategory->category_id) ? ' active' : '') ?>">
                                <?= Html::encode($category["category_name"])?>
                            </a>
                        <?php endforeach ?>

                    </div>
                </div>
            </div>
          </div>
          <div class="row-heading">
            <div class="col-sm-12">
              <h3 class="title-sm hr-left text-uppercase">Details</h3>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-12">
              <div class="icon-box bordered">
                <i class="fa fa-home fa-3x text-theme"></i>
                <h3 class="title-sm text-theme">Point of Sales</h3>
                <p class="text-theme"><?= Html::encode($market->market_address) ?></p>
              </div>
            </div>
          </div>
            <br>
            <div class="row">
                <div class="col-sm-12">
                    <div class="icon-box bordered">
                        <i class="fa fa-bullhorn fa-3x text-theme"></i>
                        <h3 class="title-sm text-theme">Contact</h3>
                        <p class="text-theme"><?= Html::encode($market->market_contacts) ?></p>
                    </div>
                </div>
            </div>
            <div class="row-heading">
                <div class="col-sm-12">
                    <h3 class="title-sm hr-left text-uppercase">Other Markets</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-default">

                        <div class="list-group">

                            <?php foreach($allMarkets as $mart): ?>
                                <a href="<?= Url::to(['market/index', 'id' => $mart->market_id])?>" class="<?= 'list-group-item' . (($mart->market_id == $market->market_id) ? ' active' : '') ?>">
                                    <?= Html::encode($mart->market_name)?>
                                </a>
                            <?php endforeach ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- MAIN CONTENT -->
        <!--===============================================================-->
        <div class="col-md-9">
            <h2 class="title-v2"><b><?= Html::encode($market->market_name) ?> </b>- <?= Html::encode($selectedCategory->category_name)?></h2>

            <?php
            for($i = 0; $i < $size; $i++){
                $item = $model[$i];

                if (($i != 0 && $i % 4 == 0)){
                    echo($row_end);
                }

                if ($i == 0 || $i % 4 == 0){
                    echo($row_begin);
                }

                $img_url = ($item['foodstuff_image_url'] == "") ? 'z_theme/assets/images/shop/500x500.gif' : ('uploads/' . $item["foodstuff_image_url"]);

                echo('<div class="col-md-4">
                    <div class="shop-box bordered">
                        <a href="#" class="img-box text-theme">
                            <img class="img-responsive" src="' . Yii::$app->urlManager->createAbsoluteUrl($img_url) .'" alt="img-theme">
                        </a>
                        <h3 class="title-sm text-theme text-theme-sm"><a href="#">' . $item["foodstuff_name"] . '</a></h3>
                        <p class="text-theme-sm"><small>' . $item["foodstuff_desc"] . '</small>
                        </p>
                        <h3 class="shop-price text-theme-sm"> &#8358;' . $item["foodstuff_market_price"] . ' / ' . $item["measure_name"] .'</h3>
                        <a class="btn btn-primary btn-tiny btn-sm text-theme-sm" href="' . Url::to(["product/item", "food_market_id" => $item["foodstuff_market_id"]]) . '"><i class="fa fa-angle-right"></i>Details</a>
                    </div>
                </div>');

                if ($i == $size - 1){
                    echo($row_end);
                }
            }
            ?>


        </div>
      </div>
</div>