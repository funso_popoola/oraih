<?php
/* @var $this yii\web\View
 * @var $model mixed
 * @var $cheapestFoodstuffs mixed
 * @var $pagination \yii\data\Pagination
 */
use yii\helpers\Url;
use yii\widgets\LinkPager;

$this->title = 'Oraih';
$sliderCount = count($cheapestFoodstuffs);
$size = count($model);
$row_begin = '<div class="row mt-20">';
$row_end = '</div>';
?>
<div class="section-intro-shop section-slider">
    <div class="layer-intro layer-intro-shop">
        <!-- SLIDER -->
        <div class="wrapper-slider" id="intro-slider-wrapper">
            <div class="carousel slide carousel-intro" id="carousel-intro" data-ride="carousel" data-interval="7500">
                <div class="wrapper-preloader">
                    <div id="preloader"></div>
                </div>
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <?php for ($i = 0; $i < $sliderCount; $i++): ?>
                        <li data-target="#carousel-intro" data-slide-to="<?= $i ?>" class="<?= (($i == 0) ? 'active' : '')?>"></li>
                    <?php endfor; ?>
                </ol>
                <!-- Wrapper for slides -->
                <div class="carousel-inner">
                    <!-- third slide -->
                    <?php for($j = 0; $j < $sliderCount; $j++):?>
                        <div class="item item-theme<?= (($j == 0) ? '-first active' : '')?>">
                            <div class="container">
                                <div class="row">
                                    <div class="col-sm-5 hidden-xs">
                                        <img class="img-responsive mt-40 img-slide-3 pull-right animation an-delay-05 an-duration-04 fadeInLeftSlider" src="<?= Yii::$app->urlManager->createAbsoluteUrl((($cheapestFoodstuffs[$j]['foodstuff_image_url'] == "") ? 'z_theme/assets/images/shop/450x280.gif' : ('uploads/' . $cheapestFoodstuffs[$j]['foodstuff_image_url'])))?>" alt="theme-img">
                                    </div>
                                    <div class="col-sm-6 col-sm-offset-1">
                                        <h1 class="text-theme title-xl mt-70 animation an-delay-08 an-duration-04 fadeInRightSlider"><?= $cheapestFoodstuffs[$j]['foodstuff_name'] ?></h1>
                                        <h3 class="text-theme title-bg animation an-delay-11 an-duration-04 fadeInUpSlider">&#8358;<?= $cheapestFoodstuffs[$j]['foodstuff_market_price'] . ' / ' . $cheapestFoodstuffs[$j]['measure_name']?></h3>
                                        <ul class="list-unstyled list-md text-theme">
                                            <li class="animation an-delay-14 an-duration-04 fadeInRightSlider"><i class="fa fa-check fa-round"></i><?= $cheapestFoodstuffs[$j]['foodstuff_desc']?></li>
                                            <li class="animation an-delay-16 an-duration-04 fadeInRightSlider"><i class="fa fa-home fa-round"></i><?= $cheapestFoodstuffs[$j]['market_address']?></li>
                                            <li class="animation an-delay-18 an-duration-04 fadeInRightSlider"><i class="fa fa-bullhorn fa-round"></i><?= $cheapestFoodstuffs[$j]['market_contacts']?></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endfor; ?>


                </div>
            </div>
        </div>
        <!-- SLIDER END-->
    </div>
    <!-- Controls -->
    <a href="<?= Url::to(['/site/index', '#' => 'carousel-intro'])?>" role="button" data-slide="prev">
        <i class="fa fa-angle-left fa-2x btn-prev-intro"></i>
    </a>
    <a href="<?= Url::to(['/site/index', '#' => 'carousel-intro'])?>" role="button" data-slide="next">
        <i class="fa fa-angle-right fa-2x btn-next-intro"></i>
    </a>
</div>

<!-- SECTION -->
<!--===============================================================-->
<div class="section section-sm section-both section-shop-items">
    <div class="container">
        <div class="row">
        </div>
        <div class="row">
            <div class="col-md-6">
                <h1 class="title-md hr-left text-uppercase">Latest Pricing</h1>
            </div>
        </div>
        <?php
        for($i = 0; $i < $size; $i++){
            $item = $model[$i];
            if ($i != 0 && $i % 4 == 0){
                echo($row_end);
            }
            if ($i == 0 || $i % 4 == 0){
                echo($row_begin);
            }

            $img_url = ($item['foodstuff_image_url'] == "") ? 'z_theme/assets/images/shop/500x500.gif' : ('uploads/' . $item["foodstuff_image_url"]);

            echo('<div class="col-md-3">
                <div class="shop-box bordered">
                    <a href="#" class="img-box text-theme">
                        <img class="img-responsive" src="' . Yii::$app->urlManager->createAbsoluteUrl($img_url) . '" alt="foodstuff-img" height="500px">
                    </a>
                    <h3 class="title-sm text-theme text-theme-sm"><a href="#">' . $item["foodstuff_name"] . '</a></h3>
                    <p class="text-theme-sm"><small>' . $item["foodstuff_desc"] . '</small>
                    </p>
                    <h3 class="shop-price text-theme-sm"> &#8358;' . $item["foodstuff_market_price"] . ' / <span>' . $item["measure_name"] . '</span></h3>
                    <a class="btn btn-primary btn-tiny btn-sm text-theme-sm" href="' . Url::to(["product/item", "food_market_id" => $item["foodstuff_market_id"]]) . '"><i class="fa fa-angle-right"></i>Details</a>
                </div>
            </div>');
        }
        ?>

    </div>
    <?= LinkPager::widget(['pagination' => $pagination]) ?>
</div>

<!-- SECTION -->
<!--===============================================================-->
<!--<div class="section section-sm section-both section-dark">
    <div class="container">
        <div class="row">
            <div class="col-sm-5">
                <img class="img-responsive" src="<?/*= Url::to('z_theme/assets/images/general/450x400.gif')*/?>" alt="theme">
            </div>
            <div class="col-sm-6 col-sm-offset-1">
                <h3 class="text-theme title-xl mt-50">AMAZING DEAL 20% OFF</h3>
                <p class="text-theme lead">PROMO CODE "BESTDEAL"</p>
                <ul class="text-theme list-unstyled list-md">
                    <li><i class="fa fa-check colored"></i>Fast Service</li>
                    <li><i class="fa fa-check colored"></i>7/24 Customer Support</li>
                    <li><i class="fa fa-check colored"></i>Experienced Team</li>
                    <li><i class="fa fa-check colored"></i>Joyful Experience</li>
                </ul>
                <a href="#" class="text-theme btn btn-lg btn-primary"><i class="fa fa-shopping-cart"></i>BUY NOW</a>
            </div>
        </div>
    </div>
</div>
-->
