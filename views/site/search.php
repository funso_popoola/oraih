<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 6/7/15
 * Time: 5:09 AM
 *
 * @var $model mixed
 */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Oraih';
$this->params['breadcrumbs'][] = $this->title;
$row_begin = '<div class="row mt-20">';
$row_end = '</div>';

$size = count($model);

?>
<div class="container mt">

    <div class="row">

        <!-- MAIN CONTENT -->
        <!--===============================================================-->
        <div class="col-md-12">
            <h2 class="title-v2">Search Result</h2>

            <?php
            for($i = 0; $i < $size; $i++){
                $item = $model[$i];
                if (($i != 0 && $i % 4 == 0)){
                    echo($row_end);
                }

                if ($i == 0 || $i % 4 == 0){
                    echo($row_begin);
                }

                $img_url = ($item['foodstuff_image_url'] == "") ? 'z_theme/assets/images/shop/500x500.gif' : ('/../uploads/' . $item["foodstuff_image_url"]);

                echo('<div class="col-md-3">
                    <div class="shop-box bordered">
                        <a href="#" class="img-box text-theme">
                            <img class="img-responsive" src="'. Yii::$app->urlManager->createAbsoluteUrl($img_url).'" alt="foodstuff-img">
                        </a>
                        <h3 class="title-sm text-theme text-theme-sm"><a href="#">' . $item["foodstuff_name"] . '</a></h3>
                        <p class="text-theme-sm"><small>' . $item["foodstuff_desc"] . '</small>
                        </p>
                        <h3 class="shop-price text-theme-sm"> &#8358;' . $item["foodstuff_market_price"] . ' / <span>' . $item["measure_name"] . '</span></h3>
                        <a class="btn btn-primary btn-tiny btn-sm text-theme-sm" href="' . Url::to(["product/item", "food_market_id" => $item["foodstuff_market_id"]]) . '"><i class="fa fa-angle-right"></i>Details</a>
                    </div>
                </div>');

                if ($i == $size - 1){
                    echo($row_end);
                }
            }
            ?>

            <?php if ($size == 0): ?>
                <div class="alert alert-info">
                    <p> Sorry, No Match Found.</p>
                    <p> You may try another search term.</p>
                    <p> Thank You!</p>
                </div>
            <?php endif; ?>

            <div class="row">
                <div class="col-sm-12">

                </div>
            </div>
        </div>
    </div>
</div>