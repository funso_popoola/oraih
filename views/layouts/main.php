<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

/* @var $this \yii\web\View */
/* @var $content string */
/* @var $identity \app\modules\admin\models\Users */

AppAsset::register($this);
$identity = Yii::$app->user->identity;
$logDisplay = 'Log In';
$logAction = '/site/login';
if (!Yii::$app->user->isGuest){
    $logDisplay = 'Logout (' . $identity->user_name . ')';
    $logAction = '/site/logout';
}

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" itemscope itemtype="http://schema.org/Event" prefix="og: http://ogp.me/ns#">
<head>
    <meta charset="<?= Yii::$app->charset ?>">

    <meta name="description" content="Oraih Foodstuff Pricing Platform"/>
    <meta name="viewport" content="width=device-width,initial-scale=1.0">

    <!-- fb meta -->
    <meta property="og:title" content="Oraih Pricing" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="http://oraih.com" />
<!--    <meta property="og:image" content="http://localhost/oraih/uploads/1433305145.jpg" />-->
    <meta property="og:description" content="Oraih Foodstuff Pricing Platform"/>

    <!-- twitter meta -->
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="@oraih">
    <meta name="twitter:creator" content="@dbox">
    <meta name="twitter:title" content="Oraih Foodstuff Pricing Platform">
    <meta name="twitter:description" content=" http://oraih.com | http://localhost/oraih/uploads/1433305145.jpg">
<!--    <meta name="twitter:image" content="http://localhost/oraih/uploads/1433305145.jpg">-->

    <!-- Add the following three tags inside head. -->
    <meta itemprop="name" content="Oraih Foodstuff Pricing Platform">
    <meta itemprop="description" content="Get the foodstuff prices across market">
<!--    <meta itemprop="image" content="http://localhost/oraih/uploads/1433305145.jpg">-->

    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="body-nav-fixed-menu-top">

<?php $this->beginBody() ?>
<!---facebook like -- -->
<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3&appId=1087439104606899";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>
<div class="wrapper-body">

    <div id="nav" class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="menu-top menu-top-inverse">
        <div class="container">
            <div class="row">
                <div class="col-sm-5 hidden-xs">
                    <a class="title-menu-top display-inline-block" href="mailto:info@oraih.com">info@oraih.com</a>
                </div>
                <div class="col-sm-7 col-xs-12">
                    <div class="pull-right">
                        <div class="pull-left">
                                <i class="fa fa-user"></i>
                                <a href="<?= Url::to([$logAction]) ?>"><?= $logDisplay ?></a>
                        </div>
                    </div>
                    <div class="list-inline social-icons-menu-top pull-right">
                        <div class="fb-like" data-href="http://staging.oraih.com/" data-width="20" data-layout="button_count" data-action="like" data-show-faces="true" data-share="false"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?= Url::home() ?>">
<!--                <img src="z_theme/assets/images/logo/logo-default.png" alt="logo">-->
            </a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                <!-- HOME DROPDOWN MENU  -->
                <li class="nav-tabs <?= (Yii::$app->requestedRoute == '' ? 'active': '' ) ?>">
                    <a href="<?= Url::home() ?>">Home</a>
                </li>
                <li class="nav-tabs <?= ((substr_count(Yii::$app->requestedRoute, 'product') > 0) ? 'active': '' ) ?>">
                    <a href="<?= Url::toRoute(['/product/index']) ?>">Products</a>
                </li>
                <li class="nav-tabs <?= ((substr_count(Yii::$app->requestedRoute, 'market') > 0) ? 'active': '' ) ?>">
                    <a href="<?= Url::toRoute(['/market/index']) ?>">Markets</a>
                </li>

                <?php if (!Yii::$app->user->isGuest): ?>
                    <li class="nav-tabs <?= ((substr_count(Yii::$app->requestedRoute, 'admin') > 0) ? 'active': '' ) ?>">
                        <a href="<?= Url::toRoute(['/admin/market/index']) ?>">Admin</a>
                    </li>
                <?php endif; ?>

                <li class="li-search-box">
                    <div class="wrapper-search-all">
                        <div class="wrapper-search-box">
                            <form class="form-searchbox" method="get" action="<?= Url::to(['/site/search'])?>" autocomplete="off">
                                <input class="input-searchbox" type="search" placeholder="Search Foodstuffs..." name="key" onkeyup="buttonUp();" required>
                                <button class="btn-searchbox" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                                <span class="icon-searchbox"></span>
                            </form>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>


    <?= $content ?>
    <div class="section footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-3 col-xs-12">
                    <p class="text-theme">Oraih. Get market and foodstuff information on our social media...</p>
                    <div class="text-theme">
                        <ul class="list-inline">
                            <li>
                                <a href="#"><i class="fa fa-facebook fa-round"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-twitter fa-round"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-linkedin fa-round"></i>
                                </a>
                            </li>

                        </ul>
                    </div>
                </div>
                <div class="col-sm-2 col-sm-offset-1 col-xs-6">
                    <h3 class="text-theme title-sm hr-left">Home</h3>
                    <ul class="list-unstyled text-theme">
                        <li>
                            <a href="<?= Url::to(['/site/index']) ?>">Latest Pricing</a>
                        </li>

                    </ul>
                </div>
                <div class="col-sm-2 col-xs-6">
                    <h3 class="text-theme title-sm hr-left">Pages</h3>
                    <ul class="list-unstyled text-theme">
                        <li>
                            <a href="<?= Url::toRoute(['/product/index'])?>">Products</a>
                        </li>
                        <li>
                            <a href="<?= Url::toRoute(['/market/index'])?>">Markets</a>
                        </li>
                        <?php if (!Yii::$app->user->isGuest): ?>
                            <li>
                                <a href="<?= Url::toRoute(['/admin/market/index']) ?>">Admin</a>
                            </li>
                        <?php endif; ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!--SECTION FOOTER BOTTOM -->
    <!--===============================================================-->
    <div class="section footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center col-footer-bottom">
                    <a id="scroll-top" href="#"><i class="fa fa-angle-up fa-2x"></i>
                    </a>
                    <p class="copyright">2015 &copy; Oraih. All Rights Reserved.</p>
                </div>
            </div>
        </div>
    </div>
</div>


    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
