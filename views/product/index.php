<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 5/20/15
 * Time: 10:14 AM
 *
 * @var $categories \app\models\Categories []
 * @var $selectedCategory \app\models\Categories
 */
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Oraih';
$this->params['breadcrumbs'][] = $this->title;
$row_begin = '<div class="row mt-20">';
$row_end = '</div>';

$currentCategory = $categories[0];
if (isset($selectedCategory)){
    $currentCategory = $selectedCategory;
}
$foodstuffs = $currentCategory->getFoodstuffs()->all();
$size = count($foodstuffs);
?>

<div class="container mt">

    <div class="row">
        <!-- SIDEBAR -->
        <!--===============================================================-->
        <div class="col-md-3">
            <div class="row-heading">
                <div class="col-sm-12">
                    <h3 class="title-sm hr-left text-uppercase">Categories</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-default">

                        <div class="list-group">

                            <?php foreach($categories as $category): ?>
                                <a href="<?= Url::to(['product/category', 'id' => $category->category_id])?>" class="<?= 'list-group-item' . (($category->category_id == $currentCategory->category_id) ? ' active' : '') ?>">
                                    <?= Html::encode($category->category_name)?>
                                    <span class="badge"><?= Html::encode($category->getFoodstuffs()->count())?></span>
                                </a>
                            <?php endforeach ?>

                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- MAIN CONTENT -->
        <!--===============================================================-->
        <div class="col-md-9">
            <h2 class="title-v2"><?= $currentCategory->category_name ?></h2>

            <?php
            for($i = 0; $i < $size; $i++){
                $item = $foodstuffs[$i];

                if (($i != 0 && $i % 4 == 0)){
                    echo($row_end);
                }

                if ($i == 0 || $i % 4 == 0){
                    echo($row_begin);
                }

                echo('<div class="col-md-4">
                        <div class="content-box bordered">

                            <h3 class="title-sm text-theme-sm">' . $item["foodstuff_name"] . '</h3>
                            <p class="text-theme-sm">' . $item["foodstuff_desc"] . '</p>
                            <a class="btn btn-primary btn-block btn-tiny btn-sm text-theme-sm" href="' . Url::to(['product/prices', 'cat_id' => $item["category_id"], 'prod_id' => $item["foodstuff_id"]]) . '">View Prices</a>
                        </div>
                    </div>');

                if ($i == $size - 1){
                    echo($row_end);
                }
            }
            ?>


        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 ">
            <p>

            </p>
            <p>

            </p>
        </div>
    </div>
</div>