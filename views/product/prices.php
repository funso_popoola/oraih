<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 5/28/15
 * Time: 2:34 PM
 *
 * @var $model mixed
 * @var $others \app\models\Foodstuffs []
 */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Oraih';
$this->params['breadcrumbs'][] = $this->title;
$row_begin = '<div class="row mt-20">';
$row_end = '</div>';

$size = count($model);
$category_name = '';
if ($size > 0)
{
    $category_name = end($model)['category_name'];
}

$othersCount = count($others);
$category = null;
if ($othersCount > 0)
{
    $category = $others[0]->getCategory()->all();
}

?>
<div class="container mt">

    <div class="row">
        <!-- SIDEBAR -->
        <!--===============================================================-->
        <div class="col-md-3">
            <div class="row-heading">
                <div class="col-sm-12">
                    <h3 class="title-sm hr-left text-uppercase"><?= $category_name ?> </h3>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-default">

                        <div class="list-group">

                            <?php foreach($others as $foodstuff): ?>
                                <a href="<?= Url::to(['product/prices', 'cat_id' => $foodstuff->category_id, 'prod_id' => $foodstuff->foodstuff_id])?>" class="<?= 'list-group-item' . (($foodstuff->foodstuff_id == $model[0]["foodstuff_id"]) ? ' active' : '')?>">
                                    <?= Html::encode($foodstuff->foodstuff_name)?>
                                </a>
                            <?php endforeach ?>

                        </div>
                    </div>
                </div>
            </div>
            <?php if ($category != null && $size > 0): ?>
                <div class="row-heading">
                    <div class="col-sm-12">
                        <h3 class="title-sm hr-left text-uppercase">Description</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="icon-box bordered">
                            <i class="fa fa-pie-chart fa-3x text-theme"></i>
                            <!--                        <h3 class="title-sm text-theme">Nutritional Make up</h3>-->
                            <p class="text-theme"><?= $model[0]['foodstuff_desc'] ?></p>
                        </div>
                    </div>
                </div>
            <?php endif; ?>


        </div>

        <!-- MAIN CONTENT -->
        <!--===============================================================-->
        <div class="col-md-9">
            <h2 class="title-v2"><?= $model[0]["foodstuff_name"] ?></h2>

            <?php
                for($i = 0; $i < $size; $i++){
                    $item = $model[$i];
                    if (($i != 0 && $i % 3 == 0)){
                        echo($row_end);
                    }

                    if ($i == 0 || $i % 3 == 0){
                        echo($row_begin);
                    }

                    $img_url = ($item['foodstuff_image_url'] == "") ? 'z_theme/assets/images/shop/500x500.gif' : ('uploads/' . $item["foodstuff_image_url"]);

                    echo('<div class="col-md-4">
                    <div class="shop-box bordered">
                        <a href="#" class="img-box text-theme">
                            <img class="img-responsive" src="'. Yii::$app->urlManager->createAbsoluteUrl($img_url).'" alt="foodstuff-img">
                        </a>
                        <h3 class="title-sm text-theme text-theme-sm"><a href="' . Url::to(['/market/index', 'id' => $item['market_id']]) .'">' . $item["market_name"] . '</a></h3>
                        <p class="text-theme-sm"><small>' . $item["market_contacts"] . '</small>
                        </p>
                        <h3 class="shop-price text-theme-sm"> &#8358;' . $item["foodstuff_market_price"] . ' / <span>' . $item["measure_name"] . '</span></h3>
                        <a class="btn btn-primary btn-tiny btn-sm text-theme-sm" href="' . Url::to(["product/item", "food_market_id" => $item["foodstuff_market_id"]]) . '"><i class="fa fa-angle-right"></i>Details</a>
                    </div>
                </div>');

                    if ($i == $size - 1){
                        echo($row_end);
                    }
                }
            ?>

            <?php if ($size <= 0): ?>
                <div class="row">
                    <div class="col-sm-12 alert alert-info">
                        <p>No Product Found!</p>
                    </div>
                </div>
            <?php endif; ?>

        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 ">
            <p>

            </p>
            <p>

            </p>
        </div>
    </div>
</div>