<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 5/20/15
 * Time: 10:42 PM
 *
 * @var $markets mixed
 * @var $selectedMarket \app\models\Markets
 * @var $tableData mixed
 * @var $foodstuff \app\models\Foodstuffs
 * @var $foodstuffMarket \app\models\FoodstuffMarkets
 * @var $measure \app\models\Measures
 */
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Oraih';
//$referral = $_SERVER['REQUEST_SCHEME'] . '://' .$_SERVER['SERVER_NAME'] . Url::to(['product/item', 'food_market_id' => $foodstuffMarket->foodstuff_market_id]);;

$referral = Yii::$app->urlManager->createAbsoluteUrl(['product/item', 'food_market_id' => $foodstuffMarket->foodstuff_market_id]);

// comment out this line on production
$referral = str_replace('localhost/oraih/web', 'staging.oraih.com', $referral);
$count = 1;
?>

<div class="section-heading-page">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1 class="heading-page"><?= $foodstuff->foodstuff_name ?></h1>
            </div>
        </div>
    </div>
</div>


<div class="container">
    <div class="row">
        <!-- SIDEBAR -->
        <!--===============================================================-->
        <div class="col-md-3">
            <div class="row-heading">
                <div class="col-sm-12">
                    <h3 class="title-sm hr-left text-uppercase">Markets</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-default">

                        <div class="list-group">

                            <?php foreach($markets as $market): ?>
                                <a href="<?= Url::to(['market/index', 'id' => $market["market_id"], 'cat_id' => $foodstuff->category_id])?>" class="<?= 'list-group-item' . (($market["market_id"] == $selectedMarket->market_id) ? ' active': '')?>">
                                    <?= Html::encode($market["market_name"])?>
                                </a>
                            <?php endforeach ?>

                        </div>

                    </div>
                </div>
            </div>

            <div class="row-heading">
                <div class="col-sm-12">
                    <h3 class="title-sm hr-left text-uppercase">More Description</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <p><?= Html::encode($foodstuff->foodstuff_desc)?></p>
                </div>
            </div>
        </div>
        <!-- MAIN CONTENT -->
        <!--===============================================================-->
        <div class="col-md-9">
            <div class="row">
                <div class="col-sm-5">
<!--                    <div class="row">-->
                        <div class="col-sm-12">
                            <!-- SLIDER -->
                            <div class="shop-item-slider">
                                <ul class="bxslider">
                                    <?php $img_url = ($foodstuff->foodstuff_image_url == "") ? 'z_theme/assets/images/shop/shop-item/500x500.gif' : ('uploads/' . $foodstuff->foodstuff_image_url); ?>
                                    <li>
                                        <img class="img-responsive" src="<?= Yii::$app->urlManager->createAbsoluteUrl($img_url) ?>" alt="theme-img">
                                    </li>

                                </ul>
                            </div>
                            <!-- SLIDER END-->
                        </div>
<!--                    </div>-->
                </div>

                <!-- CONTENT -->
                <!--===============================================================-->
                <div class="col-md-7">
                    <div class="shop-item-description">
                        <h3 class="title-lg text-theme-sm"><?= Html::encode($foodstuff->foodstuff_name) ?></h3>
                        <div class="wrapper-shop-review text-theme-sm text-theme">
                            <div>
                                <p class="lead"><strong>Supplier: </strong><?= Html::encode($selectedMarket->market_name)?></p>
                            </div>
                        </div>
                        <h3 class="title-lg text-theme title-lg-shop-item"> &#8358; <?= Html::encode($foodstuffMarket->foodstuff_market_price . ' / ' . $measure->measure_name)?></h3>
                        <ul class="text-theme list-unstyled list-md">
                            <li><i class="fa fa-home colored"></i><?= Html::encode($selectedMarket->market_address)?></li>
                            <li><i class="fa fa-bullhorn colored"></i><?= Html::encode($selectedMarket->market_contacts)?></li>
                        </ul>
                        <div class="input-group text-theme shop-item">
<!--                          <span class="input-group-btn">-->
<!--                            <button type="button" class="btn btn-primary btn-z-index"><i class="fa fa-share-alt"></i>Share Price</button>-->
<!--                          </span>-->
                            <div>
                                <!-- Share Buttons -->
                                <ul class="rrssb-buttons clearfix">
                                    <li class="rrssb-email">
                                        <a href="mailto:?subject=Foodstuff Prices From Oraih&body=Hi,%20Check%20out%20the%20price%20of%20<?= $foodstuff->foodstuff_name?>%20as%20supplied%20by%20Tejuosho%20Market%20via%20<?= $referral ?>">
                                            <span class="rrssb-icon"><svg xmlns="http://www.w3.org/2000/svg" width="28" height="28" viewBox="0 0 28 28"><path d="M20.11 26.147c-2.335 1.05-4.36 1.4-7.124 1.4C6.524 27.548.84 22.916.84 15.284.84 7.343 6.602.45 15.4.45c6.854 0 11.8 4.7 11.8 11.252 0 5.684-3.193 9.265-7.398 9.3-1.83 0-3.153-.934-3.347-2.997h-.077c-1.208 1.986-2.96 2.997-5.023 2.997-2.532 0-4.36-1.868-4.36-5.062 0-4.75 3.503-9.07 9.11-9.07 1.713 0 3.7.4 4.6.972l-1.17 7.203c-.387 2.298-.115 3.3 1 3.4 1.674 0 3.774-2.102 3.774-6.58 0-5.06-3.27-8.994-9.304-8.994C9.05 2.87 3.83 7.545 3.83 14.97c0 6.5 4.2 10.2 10 10.202 1.987 0 4.09-.43 5.647-1.245l.634 2.22zM16.647 10.1c-.31-.078-.7-.155-1.207-.155-2.572 0-4.596 2.53-4.596 5.53 0 1.5.7 2.4 1.9 2.4 1.44 0 2.96-1.83 3.31-4.088l.592-3.72z"/></svg></span>
                                            <span class="rrssb-text">email</span>
                                        </a>
                                    </li>
                                    <li class="rrssb-facebook">
                                        <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo($referral) ?>" class="popup">
                                                              <span class="rrssb-icon">
                                                                <svg xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid" width="29" height="29" viewBox="0 0 29 29">
                                                                    <path d="M26.4 0H2.6C1.714 0 0 1.715 0 2.6v23.8c0 .884 1.715 2.6 2.6 2.6h12.393V17.988h-3.996v-3.98h3.997v-3.062c0-3.746 2.835-5.97 6.177-5.97 1.6 0 2.444.173 2.845.226v3.792H21.18c-1.817 0-2.156.9-2.156 2.168v2.847h5.045l-.66 3.978h-4.386V29H26.4c.884 0 2.6-1.716 2.6-2.6V2.6c0-.885-1.716-2.6-2.6-2.6z" class="cls-2" fill-rule="evenodd"/>
                                                                </svg>
                                                              </span>
                                            <span class="rrssb-text">facebook</span>
                                        </a>
                                    </li>
                                    <li class="rrssb-twitter">
                                        <a href="https://twitter.com/home?status=Hi,%20Check%20out%20the%20price%20of%20Garri%20as%20supplied%20by%20<?= $selectedMarket->market_name ?>%20Market%20via%20<?= $referral ?>" class="popup">
                                            <span class="rrssb-icon"><svg xmlns="http://www.w3.org/2000/svg" width="28" height="28" viewBox="0 0 28 28"><path d="M24.253 8.756C24.69 17.08 18.297 24.182 9.97 24.62c-3.122.162-6.22-.646-8.86-2.32 2.702.18 5.375-.648 7.507-2.32-2.072-.248-3.818-1.662-4.49-3.64.802.13 1.62.077 2.4-.154-2.482-.466-4.312-2.586-4.412-5.11.688.276 1.426.408 2.168.387-2.135-1.65-2.73-4.62-1.394-6.965C5.574 7.816 9.54 9.84 13.802 10.07c-.842-2.738.694-5.64 3.434-6.48 2.018-.624 4.212.043 5.546 1.682 1.186-.213 2.318-.662 3.33-1.317-.386 1.256-1.248 2.312-2.4 2.942 1.048-.106 2.07-.394 3.02-.85-.458 1.182-1.343 2.15-2.48 2.71z"/></svg></span>
                                            <span class="rrssb-text">twitter</span>
                                        </a>
                                    </li>
                                    <li class="rrssb-googleplus">
                                        <!-- Replace href with your meta and URL information.  -->
                                        <a href="https://plus.google.com/share?url=<?= $referral ?>" class="popup">
                                                            <span class="rrssb-icon">
                                                              <svg xmlns="http://www.w3.org/2000/svg" width="28" height="28" viewBox="0 0 28 28">
                                                                  <path d="M14.703 15.854l-1.22-.948c-.37-.308-.88-.715-.88-1.46 0-.747.51-1.222.95-1.662 1.42-1.12 2.84-2.31 2.84-4.817 0-2.58-1.62-3.937-2.4-4.58h2.098l2.203-1.384h-6.67c-1.83 0-4.467.433-6.398 2.027C3.768 4.287 3.06 6.018 3.06 7.576c0 2.634 2.02 5.328 5.603 5.328.34 0 .71-.033 1.083-.068-.167.408-.336.748-.336 1.324 0 1.04.55 1.685 1.01 2.297-1.523.104-4.37.273-6.466 1.562-1.998 1.187-2.605 2.915-2.605 4.136 0 2.512 2.357 4.84 7.288 4.84 5.822 0 8.904-3.223 8.904-6.41.008-2.327-1.36-3.49-2.83-4.73h-.01zM10.27 11.95c-2.913 0-4.232-3.764-4.232-6.036 0-.884.168-1.797.744-2.51.543-.68 1.49-1.12 2.372-1.12 2.807 0 4.256 3.797 4.256 6.24 0 .613-.067 1.695-.845 2.48-.537.55-1.438.947-2.295.95v-.003zm.032 13.66c-3.62 0-5.957-1.733-5.957-4.143 0-2.408 2.165-3.223 2.91-3.492 1.422-.48 3.25-.545 3.556-.545.34 0 .52 0 .767.034 2.574 1.838 3.706 2.757 3.706 4.48-.002 2.072-1.736 3.664-4.982 3.648l.002.017zM23.254 11.89V8.52H21.57v3.37H18.2v1.714h3.367v3.4h1.684v-3.4h3.4V11.89"
                                                                      />
                                                              </svg>
                                                            </span>
                                            <span class="rrssb-text">google+</span>
                                        </a>
                                    </li>
                                </ul>

                                <!-- Buttons end here -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <div class="title-md text-center title-striped">
                Market Price Comparison
            </div>
            <div class="row">
<!--                <h2 class="title-v2"></h2>-->
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Market Name</th>
                        <th>Price (&#8358;)</th>
                        <th>Measure</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach($tableData as $data): ?>
                        <tr>
                            <td><?= $count++ ?></td>
                            <td><?= $data["market_name"] ?></td>
                            <td><?= $data["foodstuff_market_price"] ?></td>
                            <td><?= $data["measure_name"] ?></td>
                        </tr>
                    <?php endforeach; ?>

                    </tbody>
                </table>
            </div>

        </div>
    </div>
</div>
