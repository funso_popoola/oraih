<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 5/29/15
 * Time: 10:56 AM
 */

namespace app\models;


use yii\base\Model;
use yii\data\ActiveDataProvider;

class FoodstuffSearch extends Foodstuffs{

    public function attributes()
    {
        return array_merge(parent::attributes(), ['category.category_name']);
    }


    public function rules()
    {
        return [
            [['foodstuff_name', 'foodstuff_desc', 'category.category_name'], 'safe'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Foodstuffs::find()->innerJoinWith('category');
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10
            ],
        ]);

        $this->load($params);

        if (!$this->validate()){
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'foodstuff_name', $this->foodstuff_name])
            ->andFilterWhere(['like', 'categories.category_name', $this->getAttribute('category.category_name')])
            ->andFilterWhere(['like', 'foodstuff_desc', $this->foodstuff_desc]);

        return $dataProvider;
    }


}