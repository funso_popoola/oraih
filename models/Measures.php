<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "measures".
 *
 * @property string $measure_id
 * @property string $measure_name
 * @property string $created_at
 * @property string $modified_at
 * @property integer $active_status
 *
 * @property FoodstuffMarkets[] $foodstuffMarkets
 */
class Measures extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'measures';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['measure_name', 'created_at', 'modified_at'], 'required'],
            [['measure_name'], 'string'],
            [['created_at', 'modified_at'], 'safe'],
            [['active_status'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'measure_id' => 'Measure ID',
            'measure_name' => 'Measure Name',
            'created_at' => 'Created At',
            'modified_at' => 'Modified At',
            'active_status' => 'Active Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFoodstuffMarkets()
    {
        return $this->hasMany(FoodstuffMarkets::className(), ['foodstuff_market_price_measure_id' => 'measure_id']);
    }
}
