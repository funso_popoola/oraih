<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\web\UploadedFile;

/**
 * This is the model class for table "foodstuffs".
 *
 * @property string $foodstuff_id
 * @property string $foodstuff_name
 * @property string $category_id
 * @property string $foodstuff_desc
 * @property string $foodstuff_image_url
 * @property string $created_at
 * @property string $modified_at
 * @property integer $active_status
 *
 * @property FoodstuffMarkets[] $foodstuffMarkets
 * @property FoodstuffTags[] $foodstuffTags
 * @property Categories $category
 */
class Foodstuffs extends ActiveRecord
{
    public $file;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'foodstuffs';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['foodstuff_name', 'category_id', 'foodstuff_desc'], 'required'],
            [['foodstuff_name', 'foodstuff_desc', 'foodstuff_image_url'], 'string'],
            [['file'], 'file'],
            [['category_id', 'active_status'], 'integer'],
            [['created_at', 'modified_at'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'foodstuff_id' => 'Foodstuff ID',
            'foodstuff_name' => 'Name',
            'category_id' => 'Category ID',
            'foodstuff_desc' => 'Description',
            'foodstuff_image_url' => 'Image',
            'file' => 'Image File',
            'created_at' => 'Created At',
            'modified_at' => 'Modified At',
            'active_status' => 'Active Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFoodstuffMarkets()
    {
        return $this->hasMany(FoodstuffMarkets::className(), ['foodstuff_id' => 'foodstuff_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFoodstuffTags()
    {
        return $this->hasMany(FoodstuffTags::className(), ['foodstuff_id' => 'foodstuff_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Categories::className(), ['category_id' => 'category_id']);
    }

    public function uploadFile()
    {
        $image = UploadedFile::getInstance($this, 'file');
        if (empty($image))
        {
            return false;
        }

        $fileName = time() . '.' . $image->extension;
        $image->saveAs(Yii::$app->basePath . '/web/uploads/' . $fileName);
        $this->foodstuff_image_url = $fileName;
        return true;
    }

}
