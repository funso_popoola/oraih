<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 5/29/15
 * Time: 10:56 AM
 */

namespace app\models;


use yii\base\Model;
use yii\data\ActiveDataProvider;

class MarketSearch extends Markets{

    public function rules()
    {
        return [
            [['market_name', 'market_address', 'market_contacts'], 'safe'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Markets::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10
            ],
        ]);

        $this->load($params);

        if (!$this->validate()){
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'market_name', $this->market_name])
            ->andFilterWhere(['like', 'market_address', $this->market_address])
            ->andFilterWhere(['like', 'market_contacts', $this->market_contacts]);

        return $dataProvider;
    }
}