<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 6/1/15
 * Time: 5:53 PM
 */

namespace app\models;


use yii\base\Model;
use yii\base\Module;
use yii\data\ActiveDataProvider;

class FoodstuffMarketSearch extends FoodstuffMarkets{

    public function attributes()
    {
        return array_merge(parent::attributes(), ['foodstuff.foodstuff_name', 'foodstuffMarketPriceMeasure.measure_name']);
    }

    public function rules()
    {
        return [
            [['foodstuff_id', 'market_id', 'foodstuff_market_price', 'foodstuff_market_price_measure_id', 'foodstuff.foodstuff_name', 'foodstuffMarketPriceMeasure.measure_name'], 'safe'],
        ];
    }


    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params, $id)
    {

        $query = FoodstuffMarkets::find()
            ->innerJoinWith('foodstuff')
            ->innerJoinWith('market')
            ->innerJoinWith('foodstuffMarketPriceMeasure')
            ->where(['foodstuff_markets.market_id' => $id]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10
            ],
        ]);

        $this->load($params);
        if (!$this->validate()){
            return $dataProvider;
        }

        $query->andFilterWhere(['foodstuff_market_price' => $this->foodstuff_market_price]);
        return $dataProvider;
    }


}