<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "foodstuff_tags".
 *
 * @property string $foodstuff_tags_id
 * @property string $foodstuff_id
 * @property string $tag_id
 * @property string $created_at
 * @property string $modified_at
 * @property integer $active_status
 *
 * @property Foodstuffs $foodstuff
 * @property Tags $tag
 */
class FoodstuffTags extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'foodstuff_tags';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['foodstuff_id', 'tag_id', 'created_at', 'modified_at'], 'required'],
            [['foodstuff_id', 'tag_id', 'active_status'], 'integer'],
            [['created_at', 'modified_at'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'foodstuff_tags_id' => 'Foodstuff Tags ID',
            'foodstuff_id' => 'Foodstuff ID',
            'tag_id' => 'Tag ID',
            'created_at' => 'Created At',
            'modified_at' => 'Modified At',
            'active_status' => 'Active Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFoodstuff()
    {
        return $this->hasOne(Foodstuffs::className(), ['foodstuff_id' => 'foodstuff_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTag()
    {
        return $this->hasOne(Tags::className(), ['tag_id' => 'tag_id']);
    }
}
