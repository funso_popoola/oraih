<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\rbac\Item;

/**
 * This is the model class for table "markets".
 *
 * @property string $market_id
 * @property string $market_name
 * @property string $market_address
 * @property string $market_contacts
 * @property string $created_at
 * @property string $modified_at
 * @property integer $active_status
 *
 * @property FoodstuffMarkets[] $foodstuffMarkets
 */
class Markets extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'markets';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' =>[
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'modified_at'],
                    ActiveRecord::EVENT_AFTER_UPDATE => ['modified_at'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['market_name', 'market_address', 'market_contacts'], 'required'],
            [['market_name', 'market_address', 'market_contacts'], 'string'],
            [['created_at', 'modified_at'], 'safe'],
            [['active_status'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'market_id' => 'Market ID',
            'market_name' => 'Name',
            'market_address' => 'Address',
            'market_contacts' => 'Contacts',
            'created_at' => 'Created At',
            'modified_at' => 'Modified At',
            'active_status' => 'Active Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFoodstuffMarkets()
    {
        return $this->hasMany(FoodstuffMarkets::className(), ['market_id' => 'market_id']);
    }

    /**
     * @return \yii\db\ActiveRecord
     */
    public function getFoodstuff()
    {
        return $this->hasMany(Foodstuffs::className(), ['market_id' => 'foodstuff_id'])
            ->viaTable('foodstuff_markets', ['market_id', 'market_id']);
    }

    public static function getValidCategories($marketId)
    {
        return (new Query())
            ->distinct(true)
            ->select(['categories.*'])
            ->from('foodstuff_markets, foodstuffs, markets, categories')
            ->where('foodstuff_markets.foodstuff_id = foodstuffs.foodstuff_id')
            ->andWhere('foodstuff_markets.market_id = markets.market_id')
            ->andWhere(['foodstuff_markets.market_id' => $marketId])
            ->andWhere('categories.category_id = foodstuffs.category_id')
            ->all();
    }

    public static function getMarketByCategoryId($marketId, $categoryId)
    {
        return (new Query())
            ->select(['foodstuff_markets.*', 'foodstuffs.*', 'markets.*', 'measures.*', 'categories.*'])
            ->from('foodstuff_markets, foodstuffs, markets, measures, categories')
            ->where('foodstuff_markets.foodstuff_id = foodstuffs.foodstuff_id')
            ->andWhere('foodstuff_markets.market_id = markets.market_id')
            ->andWhere(['foodstuff_markets.market_id' => $marketId])
            ->andWhere('measures.measure_id=foodstuff_markets.foodstuff_market_price_measure_id')
            ->andWhere('categories.category_id = foodstuffs.category_id')
            ->andWhere(['foodstuffs.category_id' => $categoryId])
            ->all();
    }
}
