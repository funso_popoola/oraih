<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\web\UploadedFile;

/**
 * This is the model class for table "foodstuff_markets".
 *
 * @property string $foodstuff_market_id
 * @property string $foodstuff_id
 * @property string $market_id
 * @property double $foodstuff_market_price
 * @property string $foodstuff_market_price_measure_id
 * @property integer $stock_status
 * @property string $created_at
 * @property string $modified_at
 * @property integer $active_status
 *
 * @property Foodstuffs $foodstuff
 * @property Measures $foodstuffMarketPriceMeasure
 * @property Markets $market
 */
class FoodstuffMarkets extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'foodstuff_markets';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['foodstuff_id', 'market_id', 'foodstuff_market_price', 'foodstuff_market_price_measure_id'], 'required'],
            [['foodstuff_id', 'market_id', 'foodstuff_market_price_measure_id', 'stock_status', 'active_status'], 'integer'],
            [['foodstuff_market_price'], 'number'],
            [['created_at', 'modified_at'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'foodstuff_market_id' => 'Foodstuff Market ID',
            'foodstuff_id' => 'Foodstuff ID',
            'market_id' => 'Market ID',
            'foodstuff_market_price' => 'Price',
            'foodstuff_market_price_measure_id' => 'Foodstuff Market Price Measure ID',
            'stock_status' => 'Stock Status',
            'created_at' => 'Created At',
            'modified_at' => 'Modified At',
            'active_status' => 'Active Status',
        ];
    }

    public function isDuplicate()
    {
        return self::find()
            ->where(['foodstuff_id' => $this->foodstuff_id])
            ->andWhere(['market_id' => $this->market_id])
            ->andWhere(['foodstuff_market_price_measure_id' => $this->foodstuff_market_price_measure_id])
            ->exists();
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFoodstuff()
    {
        return $this->hasOne(Foodstuffs::className(), ['foodstuff_id' => 'foodstuff_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFoodstuffMarketPriceMeasure()
    {
        return $this->hasOne(Measures::className(), ['measure_id' => 'foodstuff_market_price_measure_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMarket()
    {
        return $this->hasOne(Markets::className(), ['market_id' => 'market_id']);
    }

    public static function getCheapestFoodstuffs($limit)
    {
        return (new Query())
            ->select(['foodstuff_markets.*', 'foodstuffs.*', 'categories.*', 'measures.*', 'markets.*'])
            ->from(['foodstuff_markets', 'foodstuffs', 'categories', 'measures', 'markets'])
            ->where('foodstuff_markets.foodstuff_id = foodstuffs.foodstuff_id')
            ->andWhere('foodstuff_markets.market_id = markets.market_id')
            ->andWhere('foodstuffs.category_id=categories.category_id')
            ->andWhere('foodstuff_markets.foodstuff_market_price_measure_id=measures.measure_id')
            ->orderBy(['foodstuff_markets.foodstuff_market_price' => SORT_ASC])
            ->groupBy('foodstuff_markets.foodstuff_id')
            ->limit($limit)
            ->all();
    }

    public static function searchFoodstuffs($keyword)
    {
        return (new Query())
            ->distinct()
            ->select(['foodstuff_markets.*', 'foodstuffs.*', 'categories.*', 'measures.*', 'markets.*'])
            ->from(['foodstuff_markets', 'foodstuffs', 'categories', 'measures', 'markets'])
            ->where('foodstuff_markets.foodstuff_id = foodstuffs.foodstuff_id')
            ->andWhere('foodstuff_markets.market_id = markets.market_id')
            ->andWhere('foodstuffs.category_id = categories.category_id')
            ->andWhere('foodstuff_markets.foodstuff_market_price_measure_id=measures.measure_id')
            ->andfilterWhere(['like', 'foodstuffs.foodstuff_name',  $keyword])
            ->orFilterWhere(['like', 'categories.category_name', $keyword])
            ->limit(12)
            ->all();
    }

    public static function getFoodstuffPrices($productId)
    {
        return (new Query())
            ->select(['foodstuff_markets.*', 'foodstuffs.*', 'markets.*', 'measures.*', 'categories.*'])
            ->from('foodstuff_markets, foodstuffs, markets, measures, categories')
            ->where('foodstuff_markets.foodstuff_id=foodstuffs.foodstuff_id')
            ->andWhere('foodstuff_markets.market_id = markets.market_id')
            ->andWhere('measures.measure_id=foodstuff_markets.foodstuff_market_price_measure_id')
            ->andWhere('foodstuffs.category_id = categories.category_id')
            ->andWhere(['foodstuff_markets.foodstuff_id' => $productId])
            ->all();
    }

    public static function getSpecificSuppliers($foodstuffId)
    {
        return (new Query())
            ->distinct(true)
            ->select(['markets.*'])
            ->from('markets, foodstuff_markets, foodstuffs')
            ->where('foodstuff_markets.foodstuff_id = foodstuffs.foodstuff_id')
            ->andWhere('foodstuff_markets.market_id = markets.market_id')
            ->andWhere(['foodstuff_markets.foodstuff_id' => $foodstuffId])
            ->all();
    }

    public static function getFoodstuffPriceCompare($foodstuffId)
    {
        return (new Query())
            ->select(['markets.*', 'foodstuff_markets.*', 'measures.*'])
            ->from('markets, foodstuff_markets, foodstuffs, measures')
            ->where('foodstuff_markets.foodstuff_id = foodstuffs.foodstuff_id')
            ->andWhere('foodstuff_markets.market_id = markets.market_id')
            ->andWhere(['foodstuff_markets.foodstuff_id' => $foodstuffId])
            ->andWhere('measures.measure_id = foodstuff_markets.foodstuff_market_price_measure_id')
            ->all();
    }
}
