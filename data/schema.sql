CREATE DATABASE IF NOT EXISTS oraih;

USE oraih;

CREATE TABLE IF NOT EXISTS user_types(
  user_type_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
  user_type_name VARCHAR(350) NOT NULL ,
  created_at DATETIME NOT NULL ,
  modified_at DATETIME NOT NULL ,
  active_status BOOLEAN DEFAULT 1,
  PRIMARY KEY (user_type_id)
);

CREATE TABLE IF NOT EXISTS users(
  user_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
  user_name TEXT NOT NULL ,
  user_email VARCHAR(250) NOT NULL ,
  user_password VARCHAR(250) NOT NULL ,
  user_type_id INT UNSIGNED NOT NULL ,
  created_at DATETIME NOT NULL ,
  modified_at DATETIME NOT NULL ,
  active_status BOOLEAN DEFAULT 1,
  PRIMARY KEY (user_id),
  CONSTRAINT fk_users_user_type_id FOREIGN KEY (user_type_id) REFERENCES user_types(user_type_id)
);

CREATE TABLE IF NOT EXISTS markets(
  market_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
  market_name TEXT NOT NULL ,
  market_address TEXT NOT NULL ,
  market_contacts TEXT NOT NULL ,
  created_at DATETIME NOT NULL ,
  modified_at DATETIME NOT NULL ,
  active_status BOOLEAN DEFAULT 1,
  PRIMARY KEY (market_id)
);

CREATE TABLE IF NOT EXISTS categories(
  category_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
  category_name VARCHAR(350) NOT NULL ,
  created_at DATETIME NOT NULL ,
  modified_at DATETIME NOT NULL ,
  active_status BOOLEAN DEFAULT 1,
  PRIMARY KEY (category_id)
);

CREATE TABLE IF NOT EXISTS foodstuffs(
  foodstuff_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
  foodstuff_name TEXT NOT NULL ,
  category_id INT UNSIGNED NOT NULL ,
  foodstuff_desc TEXT NOT NULL ,
  created_at DATETIME NOT NULL ,
  modified_at DATETIME NOT NULL ,
  active_status BOOLEAN DEFAULT 1,
  PRIMARY KEY (foodstuff_id),
  CONSTRAINT fk_foodstuffs_category_id FOREIGN KEY (category_id) REFERENCES categories(category_id)
);

CREATE TABLE IF NOT EXISTS measures(
  measure_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
  measure_name TEXT NOT NULL ,
  created_at DATETIME NOT NULL ,
  modified_at DATETIME NOT NULL ,
  active_status BOOLEAN DEFAULT 1,
  PRIMARY KEY (measure_id)
);

CREATE TABLE IF NOT EXISTS foodstuff_markets(
  foodstuff_market_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
  foodstuff_id INT UNSIGNED NOT NULL ,
  market_id INT UNSIGNED NOT NULL ,
  foodstuff_market_image_url TEXT NOT NULL ,
  foodstuff_market_price DOUBLE NOT NULL ,
  foodstuff_market_price_measure_id INT UNSIGNED NOT NULL ,
  stock_status BOOLEAN DEFAULT 1,
  created_at DATETIME NOT NULL ,
  modified_at DATETIME NOT NULL ,
  active_status BOOLEAN DEFAULT 1,
  PRIMARY KEY (foodstuff_market_id),
  UNIQUE (foodstuff_id, market_id, foodstuff_market_price_measure_id),
  CONSTRAINT fk_foodstuff_markets_foodstuff_id FOREIGN KEY (foodstuff_id) REFERENCES foodstuffs(foodstuff_id),
  CONSTRAINT fk_foodstuff_markets_market_id FOREIGN KEY (market_id) REFERENCES markets(market_id),
  CONSTRAINT fk_foodstuff_markets_foodstuff_market_price_measure_id FOREIGN KEY (foodstuff_market_price_measure_id) REFERENCES measures(measure_id)
);

CREATE TABLE IF NOT EXISTS tags(
  tag_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
  tag_name VARCHAR(250) NOT NULL ,
  created_at DATETIME NOT NULL ,
  modified_at DATETIME NOT NULL ,
  active_status BOOLEAN DEFAULT 1,
  PRIMARY KEY (tag_id)
);

CREATE TABLE IF NOT EXISTS foodstuff_tags(
  foodstuff_tags_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
  foodstuff_id INT UNSIGNED NOT NULL ,
  tag_id INT UNSIGNED NOT NULL ,
  created_at DATETIME NOT NULL ,
  modified_at DATETIME NOT NULL ,
  active_status BOOLEAN DEFAULT 1,
  PRIMARY KEY (foodstuff_tags_id),
  CONSTRAINT fk_foodstuff_tags_foodstuff_id FOREIGN KEY (foodstuff_id) REFERENCES foodstuffs(foodstuff_id),
  CONSTRAINT fk_foodstuff_tags_tag_id FOREIGN KEY (tag_id) REFERENCES tags(tag_id)
);
