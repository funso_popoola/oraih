USE oraih;

INSERT INTO user_types(user_type_id, user_type_name, created_at, modified_at, active_status) VALUES
  (1, "admin", NOW(), NOW(), 1),
  (2, "sub-admin", NOW(), NOW(), 2);

INSERT INTO categories(category_id, category_name, created_at, modified_at, active_status) VALUES
  (1, "General", NOW(), NOW(), 1),
  (2, "Grains", NOW(), NOW(), 1),
  (3, "Tuber Crops", NOW(), NOW(), 1),
  (4, "Vegetables", NOW(), NOW(), 1),
  (5, "Beverages", NOW(), NOW(), 1),
  (6, "Oil", NOW(), NOW(), 1),
  (7, "Fruits", NOW(), NOW(), 1);

INSERT INTO markets(market_id, market_name, market_address, market_contacts, created_at, modified_at, active_status) VALUES
  (1, "One", "Block 22, Aderemi Road, Ile-Ife. Osun State", "marketone@oraih.com", NOW(), NOW(), 1),
  (2, "Two", "Block 22, Aderemi Road, Ile-Ife. Osun State", "marketone@oraih.com", NOW(), NOW(), 1),
  (3, "Three", "Block 22, Aderemi Road, Ile-Ife. Osun State", "marketone@oraih.com", NOW(), NOW(), 1),
  (4, "Four", "Block 22, Aderemi Road, Ile-Ife. Osun State", "marketone@oraih.com", NOW(), NOW(), 1);

INSERT INTO measures(measure_id, measure_name, created_at, modified_at, active_status) VALUES
  (1, "Cup", NOW(), NOW(), 1),
  (2, "Bowl", NOW(), NOW(), 1),
  (3, "Bag", NOW(), NOW(), 1),
  (4, "Tuber", NOW(), NOW(), 1),
  (5, "Gallon", NOW(), NOW(), 1),
  (6, "Bottle", NOW(), NOW(), 1),
  (7, "Bunch", NOW(), NOW(), 1),
  (8, "Unit", NOW(), NOW(), 1);

INSERT INTO foodstuffs(foodstuff_id, foodstuff_name, category_id, foodstuff_desc, created_at, modified_at, active_status) VALUES
  (1, "Garri", 1, "The popular cassava flakes", NOW(), NOW(), 1),
  (2, "Beans", 2, "Nigerian most common legume", NOW(), NOW(), 1),
  (3, "Soya Beans", 2, "Another Legume", NOW(), NOW(), 1),
  (4, "Palm Oil", 6, "The cooking oil extracted from palm kernel", NOW(), NOW(), 1),
  (5, "Bitter Leaf", 4, "Generally known as ewuro", NOW(), NOW(), 1),
  (6, "Water Melon", 7, "Good fruit", NOW(), NOW(), 1),
  (7, "Banana", 7, "The Banana", NOW(), NOW(), 1);

INSERT INTO foodstuff_markets(foodstuff_id, market_id, foodstuff_market_image_url, foodstuff_market_price, foodstuff_market_price_measure_id, stock_status, created_at, modified_at, active_status) VALUES
  (1, 1, "", 150, 2, 1, NOW(), NOW(), 1),
  (2, 2, "", 350, 2, 1, NOW(), NOW(), 1),
  (1, 3, "", 15, 1, 1, NOW(), NOW(), 1),
  (1, 4, "", 160, 2, 1, NOW(), NOW(), 1),
  (1, 1, "", 20, 1, 1, NOW(), NOW(), 1),
  (3, 3, "", 550, 2, 1, NOW(), NOW(), 1),
  (3, 2, "", 150, 2, 1, NOW(), NOW(), 1),
  (4, 4, "", 250, 5, 1, NOW(), NOW(), 1),
  (7, 3, "", 120, 7, 1, NOW(), NOW(), 1),
  (2, 2, "", 40, 1, 1, NOW(), NOW(), 1),
  (6, 3, "", 150, 8, 1, NOW(), NOW(), 1),
  (5, 1, "", 90, 7, 1, NOW(), NOW(), 1),
  (2, 4, "", 15000, 3, 1, NOW(), NOW(), 1),
  (5, 2, "", 60, 7, 1, NOW(), NOW(), 1),
  (7, 4, "", 150, 7, 1, NOW(), NOW(), 1),
  (1, 3, "", 140, 2, 1, NOW(), NOW(), 1),
  (6, 4, "", 100, 8, 1, NOW(), NOW(), 1),
  (4, 2, "", 250, 5, 1, NOW(), NOW(), 1),
  (3, 4, "", 70, 1, 1, NOW(), NOW(), 1),
  (2, 1, "", 400, 2, 1, NOW(), NOW(), 1),
  (4, 1, "", 220, 5, 1, NOW(), NOW(), 1),
  (5, 3, "", 50, 7, 1, NOW(), NOW(), 1),
  (7, 2, "", 100, 7, 1, NOW(), NOW(), 1),
  (6, 1, "", 120, 8, 1, NOW(), NOW(), 1);



